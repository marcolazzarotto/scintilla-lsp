#include "editorwindow.h"
#include "../qt/ScintillaLsp/LspScintillaEdit.h"
#include "../lsp/lspclient.h"
#include "../lsp/lspstructures.h"
#include "../lsp/ILspDocument.h"

#include <QApplication>
#include <QTemporaryFile>

enum
{
	IndicLspDiagnosticHint = INDICATOR_CONTAINER,
	IndicLspDiagnosticInfo = INDICATOR_CONTAINER + 1,
	IndicLspDiagnosticWarning = INDICATOR_CONTAINER + 2,
	IndicLspDiagnosticError = INDICATOR_CONTAINER + 3
};

inline int scintillaColor(const QColor &col)
{
	return (col.blue() << 16) | (col.green() << 8) | col.red();
}

EditorWindow::EditorWindow(QWidget *parent)
	: QMainWindow(parent)
{
	setWindowTitle("Scintilla LSP demo");
	setMinimumSize(800, 600);
	// Create a temporary file
	QTemporaryFile tf(QStringLiteral("tempXXXXXX.cpp"));
	tf.open();
	const QString uri = QStringLiteral("file:///%1").arg(tf.fileName());
	// Create editor and add as central widget
	auto editor = new LspScintillaEdit;
	setCentralWidget(editor);
	// Create LSP server
	auto client = new Scintilla::LspClient();
	editor->setupLspClient(client);
	// Initialize and start LSP server
	client->lspStartServer("clangd.exe", "");
	Scintilla::LspClientConfiguration cfg;
	cfg.rootUri = QStringLiteral("file:///%1").arg(QApplication::applicationDirPath()).toStdString();
	cfg.semanticHighlighting.supported = true;	// Default not supported for now, handling is buggy
	client->lspInitialize(cfg);
	// Add document to LSP server
	client->addDocument(Scintilla::LspScintillaDoc(editor->docPointer()), uri.toUtf8().data(), "cpp");
	// Delete client before quitting application
	QObject::connect(qApp, &QApplication::aboutToQuit, [=]() {
		delete client;
		QCoreApplication::processEvents(QEventLoop::AllEvents | QEventLoop::WaitForMoreEvents, 2000);
	});

	//
	//	Editor configuration
	//
	editor->styleSetFont(STYLE_DEFAULT, "consolas");
	editor->styleSetSize(STYLE_DEFAULT, 12);
	editor->indicSetStyle(IndicLspDiagnosticError, INDIC_SQUIGGLE);
	editor->indicSetStyle(IndicLspDiagnosticWarning, INDIC_DIAGONAL);
	editor->indicSetStyle(IndicLspDiagnosticInfo, INDIC_TT);
	editor->indicSetStyle(IndicLspDiagnosticHint, INDIC_DOTS);
	// Styles for LSP
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle, scintillaColor(QColor(0xFF0000)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 1, scintillaColor(QColor(0x880000)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 2, scintillaColor(QColor(0x440000)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 3, scintillaColor(QColor(0x00FF00)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 4, scintillaColor(QColor(0x008800)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 5, scintillaColor(QColor(0x004400)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 6, scintillaColor(QColor(0x0000FF)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 7, scintillaColor(QColor(0x000088)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 8, scintillaColor(QColor(0x008888)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 9, scintillaColor(QColor(0xFF00FF)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 10, scintillaColor(QColor(0x8800FF)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 11, scintillaColor(QColor(0x4400FF)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 12, scintillaColor(QColor(0xFF0088)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 13, scintillaColor(QColor(0x00FFFF)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 14, scintillaColor(QColor(0x00FF88)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 15, scintillaColor(QColor(0x00FF44)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 16, scintillaColor(QColor(0x0088FF)));
	editor->styleSetFore(Scintilla::LspClient::FirstLspStyle + 17, scintillaColor(QColor(0xFF0088)));

	// NOTE: this variable is here for demonstration, it should be part of editor
	Scintilla::LspCompletionList autoCompletion;

	//
	//	Handle autocompletion start
	//
	connect(editor, &LspScintillaEdit::lspCompletion, [=, &autoCompletion](const Scintilla::LspCompletionList &cl) {
		autoCompletion = cl;
		if (cl.items.size() == 0)
		{
			editor->autoCCancel();
			return;
		}
		QStringList items;
		for (auto i : cl.items)
			items.append(QString::fromStdString(i.label).simplified());
		editor->autoCSetSeparator('\t');
		editor->autoCShow(0, items.join("\t").toUtf8().constData());
	});
	//
	//	Handle autocompletion apply
	//
	connect(editor, &ScintillaEdit::autoCompleteSelection, [=, &autoCompletion](int position, const QString &text, QChar fillupChar, int listCompletionMethod) {
		Q_UNUSED(position)
		Q_UNUSED(text)
		Q_UNUSED(fillupChar)
		Q_UNUSED(listCompletionMethod)
		// Prendo l'indice selezionato e cancello la lista
		const int curIdx = editor->autoCCurrent();
		if (curIdx < 0 || curIdx >= autoCompletion.items.size())
			return;
		editor->autoCCancel();
		// Eseguo l'autocompletamento
		const auto &textEdit = autoCompletion.items.at(curIdx).textEdit;
		Scintilla::LspScintillaDoc doc(editor->docPointer());
		Sci_Position p_start, p_end;
		if (!Scintilla::lspConv::convertRange(doc, textEdit.range, p_start, p_end))
			return;
		editor->setSel(p_start, p_end);
		editor->replaceSel(textEdit.newText.c_str());
	});
	//
	//	Handle signature help
	//
	connect(editor, &LspScintillaEdit::lspSignatureHelp, [=](const Scintilla::LspSignatureHelp &help) {
		const std::size_t sz = help.signatures.size();
		if (!hasFocus() || (sz == 0))
			return;
		QByteArray text;
		Sci_Position hlt_start = 0, hlt_end = 0;
		if (sz == 1)
			text = QByteArray::fromStdString(help.signatures[0].label);
		else for (int i = 0; i < sz; i++)
		{
			const int idx = (help.activeSignature + i) % sz;
			if (!text.isEmpty()) text.append('\n');
			text.append("\001[");
			text.append(QByteArray::number(idx + 1));
			text.append("]\002 ");
			if (idx == help.activeSignature)
			{
				const auto &par = help.signatures[idx].parameters[help.activeParameter];
				hlt_start = text.size() + par.label_start;
				hlt_end = hlt_start + (par.label_end - par.label_start);
			}
			text.append(help.signatures[idx].label.c_str());
		}
		editor->callTipShow(editor->currentPos(), text.constData());
		editor->callTipSetHlt(hlt_start, hlt_end);
	});
	//
	//	Handle document diagnostics
	//
	connect(editor, &LspScintillaEdit::lspDiagnostic, [=](const Scintilla::LspDocumentDiagnostic &dd) {
		const auto docLen = editor->length();
		// Clear indicators used for diagnostics
		editor->setIndicatorCurrent(IndicLspDiagnosticHint);
		editor->indicatorClearRange(0, docLen);
		editor->setIndicatorCurrent(IndicLspDiagnosticInfo);
		editor->indicatorClearRange(0, docLen);
		editor->setIndicatorCurrent(IndicLspDiagnosticWarning);
		editor->indicatorClearRange(0, docLen);
		editor->setIndicatorCurrent(IndicLspDiagnosticError);
		editor->indicatorClearRange(0, docLen);
		const Scintilla::LspScintillaDoc doc(editor->docPointer());
		for (const auto &e : dd)
		{
			const Sci_Position posFrom = Scintilla::lspConv::sciPos(doc, e.range.start);
			const Sci_Position posTo = Scintilla::lspConv::sciPos(doc, e.range.end);
			//
			//	Set indicator
			//
			switch (e.severity)
			{
				case Scintilla::LspDiagnosticElement::Hint:
					editor->setIndicatorCurrent(IndicLspDiagnosticHint);
					break;
				case Scintilla::LspDiagnosticElement::Information:
					editor->setIndicatorCurrent(IndicLspDiagnosticInfo);
					break;
				case Scintilla::LspDiagnosticElement::Warning:
					editor->setIndicatorCurrent(IndicLspDiagnosticWarning);
					break;
				case Scintilla::LspDiagnosticElement::Error:
					editor->setIndicatorCurrent(IndicLspDiagnosticError);
					break;
			}
			editor->indicatorFillRange(posFrom, posTo - posFrom);
		}
	});
}

EditorWindow::~EditorWindow()
{
}
