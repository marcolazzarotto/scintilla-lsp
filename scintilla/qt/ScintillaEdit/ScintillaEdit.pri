#
#	Full Qt Scintilla editor
#

include(../ScintillaEditBase/ScintillaEditBase.pri)

SOURCES += \
	$${PWD}/ScintillaEdit.cpp \
	$${PWD}/ScintillaDocument.cpp \

HEADERS  += \
	$${PWD}/ScintillaEdit.h \
	$${PWD}/ScintillaDocument.h \
